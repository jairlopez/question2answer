<?php
/*
	This is a modified version (see CHANGELOG.md) of:

	Question2Answer - Q&A platform - https://www.question2answer.org/
	Copyright (C) 2011-2020 Gideon Greenspan and contributors

	File: qa-plugin/opensearch-support/qa-plugin.php
	Description: Initiates OpenSearch plugin


	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see
	<https://gitlab.com/jairlopez/question2answer/-/blob/master/LICENSE.md>.
*/

/*
	Plugin Name: OpenSearch Support
	Plugin URI:
	Plugin Description: Allows OpenSearch clients to search Q2A site directly
	Plugin Version: 1.0
	Plugin Date: 2012-08-21
	Plugin Author: Question2Answer
	Plugin Author URI: https://www.question2answer.org/
	Plugin License: GPLv3+
	Plugin Minimum Question2Answer Version: 1.5
	Plugin Update Check URI:
*/


if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}


qa_register_plugin_layer('qa-opensearch-layer.php', 'OpenSearch Layer');
qa_register_plugin_module('page', 'qa-opensearch-page.php', 'qa_opensearch_xml', 'OpenSearch XML');
