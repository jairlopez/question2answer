<?php
/*
	This is a modified version (see CHANGELOG.md) of:

	Question2Answer - Q&A platform - https://www.question2answer.org/
	Copyright (C) 2011-2020 Gideon Greenspan and contributors

	File: qa-plugin/opensearch-support/qa-opensearch-layer.php
	Description: Theme layer class for OpenSearch plugin


	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see
	<https://gitlab.com/jairlopez/question2answer/-/blob/master/LICENSE.md>.
*/

class qa_html_theme_layer extends qa_html_theme_base
{
	public function head_links()
	{
		qa_html_theme_base::head_links();

		$this->output('<link rel="search" type="application/opensearchdescription+xml" title="' . qa_html(qa_opt('site_title')) . '" href="' . qa_path_html('opensearch.xml') . '"/>');
	}
}
