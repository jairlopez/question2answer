Question2Answer
---------------

**WARNING:** This is a modified version of
Question2Answer (see
[CHANGELOG.md](CHANGELOG.md));
documentation is available [here](https://jairlopez.gitlab.io/question2answer-doc).


[Question2Answer][Q2A] (Q2A) is a popular free open source Q&A platform for
PHP/MySQL, used by over 20,898 [sites](https://www.question2answer.org/sites.php) in 40 languages.


Q2A is highly customisable with many awesome features:

- Asking and answering questions (duh!)
- Voting, comments, best answer selection, follow-on and closed questions
- Complete user management including points-based reputation management
- Create experts, editors, moderators and admins
- Fast integrated search engine, plus checking for similar questions when asking
- Categories (up to 4 levels deep) and/or tagging
- Easy styling with CSS themes
- Supports translation into any language
- Custom sidebar, widgets, pages and links
- SEO features such as neat URLs, microformats and XML Sitemaps
- RSS, email notifications and personal news feeds
- User avatars (or Gravatar) and custom fields
- Private messages and public wall posts
- Log in via Facebook or others (using plugins)
- Out-of-the-box WordPress 3+ integration
- Out-of-the-box Joomla! 3.0+ integration (in conjunction with a Joomla! extension)
- Custom single sign-on support for other sites
- PHP/MySQL scalable to millions of users and posts
- Safe from XSS, CSRF and SQL injection attacks
- Beat spam with captchas, rate-limiting, moderation and/or flagging
- Block users, IP addresses, and censor words

Q2A also features an extensive plugin system:

- Modify the HTML output for a page with *layers*
- Add custom pages to a Q2A site with *page modules*
- Add extra content in various places with *widget modules*
- Allow login via an external identity provider such as Facebook with *login modules*
- Integrate WYSIWYG or other text editors with *editor/viewer modules*
- Do something when certain actions take place with *event modules*
- Validate and/or modify many types of user input with *filter modules*
- Implement a custom search engine with *search modules*
- Add extra spam protection with *captcha modules*
- Extend many core Q2A functions using *function overrides*


## Rationale for this repository

* Give our contribution to the Question2Answer community

* Allow Question2Answer's official owners and mantainers to pick the
  modifications they consider apropriate to merge into Question2Answer itself,
  without overloading them

* Offer an up-to-date platform and documentation to users, developers,
  designers, and experts

[Q2A]: https://www.question2answer.org/
