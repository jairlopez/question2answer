# Change Log

## 1.8.8-revision.1.0.0

- *Date:* May 20th, 2024
- *Development base:* Q2A v1.8.8


### Changes

- Temporarily disable testing; because there will be a major refactor in
  upcoming commits.

- Update link to PHPCS; so that it is compatible with PHP >= 8.

- Upstream: Update PHPMailer from `5.2.28` to `6.6.3`; so that it is compatible
  with PHP >= 8.

- Drop support for PHP < 8.


### Bug fixes

- [Upstream-@9a0d117](https://github.com/q2a/question2answer/commit/9a0d1175e398315442a406df9161cedead9848c2):
  Include query parameters when redirecting incorrect question URL.

- [Upstream-#906](https://github.com/q2a/question2answer/pull/906):
  Fix #895 - Images not loading properly in PHP 8.

- [Upstream-#909](https://github.com/q2a/question2answer/pull/909):
  Fix #895 - Images not loading properly in PHP 8.

- [Upstream-#910](https://github.com/q2a/question2answer/pull/910):
  Significant performance improvements.

- [Upstream-#924](https://github.com/q2a/question2answer/pull/924):
  Fix captcha on feedback page not being shown for anonymous users.

- [Upstream-#932](https://github.com/q2a/question2answer/pull/932):
  Allow pagination of element lists that exceed QA_MAX_LIMIT_START.

- [Upstream-#948](https://github.com/q2a/question2answer/pull/948):
  Fix many string management functions in PHP 8.1.

- [Upstream-#949](https://github.com/q2a/question2answer/pull/949):
  Invalidate cache when performing additional post operations.

- [Upstream-#950](https://github.com/q2a/question2answer/pull/950):
  Redirect if requested URL params exceed real results.

- [Upstream-#954](https://github.com/q2a/question2answer/pull/954):
  Avoid joining with users table to get userid several times on user profile
  page.

- [Upstream-#966](https://github.com/q2a/question2answer/pull/966):
  Updates to urlencode() and urldecode() functions.

- [Upstream-#972](https://github.com/q2a/question2answer/pull/972):
  Avoid SMTP credentials from being autofilled with site credentials.

- [Upstream-#976](https://github.com/q2a/question2answer/pull/976):
  Fix wrong validation of profile field length.

- [Upstream-#992](https://github.com/q2a/question2answer/pull/992):
  Fix interpretation of plus signs.


## 1.8.6-revision.1.0.0

- *Date:* July 16th, 2020
- *Development base:* Q2A v1.8.5


### Additions

- Mention `password_compat` component in LICENSE.md


### Changes

- Update htmLawed from `1.2.4.1`to `1.2.5`
- Upstream: Update CKEditor from `4.11.1` to `4.14.1`
- Upstream: Update jQuery from `3.3.1` to `3.5.1`
- Upstream: Update PHPMailer from `5.2.26` to `5.2.28`
- [Upstream-#740](https://github.com/q2a/question2answer/pull/740):
  Sanitize HTML `style` tag attribute when posting questions, answers and
  comments
- [Upstream-#754](https://github.com/q2a/question2answer/pull/754):
  Remove bookmark entries about deleted users, alongside their own
  bookmarked users; when deleting users. It'll save some database space
- [Upstream-#745](https://github.com/q2a/question2answer/issues/745):
  Add 404 response for empty tag page
- [Upstream-#781](https://github.com/q2a/question2answer/issues/781) Improve
  compact numbers
- Upstream: Remove unnecessary transitions in SnowFlat theme
- Upstream: Improve delete cache performance Delete more files per AJAX request


### Bug fixes

- Fix PHP notices shown
    - When visiting non-existent tag pages
    - When visiting non-existent single question pages
    - When questions get closed because it has a selected answer and the option
      '`Close questions with a selected answer`' in on; this setting is under
      '`Admin`' > '`Posting`'.
- [Uptream-#758](https://github.com/q2a/question2answer/pull/758):
  Fix database overflow issue when trying to insert content words
- [Upstream-#812](https://github.com/q2a/question2answer/issues/812):
  Prevent invisible characters in usernames
- [Upstream-#808](https://github.com/q2a/question2answer/issues/808):
  Move category dropdown higher to prevent browser bug.  Browsers don't move
  `<select>` dropdowns when the page shifts.
- [Upstream-#785](https://github.com/q2a/question2answer/issues/785):
  Prevent parsing links in code blocks
- Upstream: Improve delete cache performance Delete more files per AJAX request


## 1.8.5-revision.1.0.0

*Date:* July 8th, 2020.

- Clone Question2Answer 1.8.4 as 1.8.5-revision.1.0.0
- Use GPLv3+, instead of GPLv2+ so that it's possible to add components
  under The Apache License 2.0
- Remove fonts that aren't compatible with GPL
    - Replace Fontello with Font Awesome
    - Offload Font Awesome loading onto CloudFlare's CDN
    - Offload Ubuntu font loading onto Google Fonts
- Remove `CHANGELOG.html`, `LICENSE.html`, and `README.html`
- Add `LICENSE.md` and `CHANGELOG.md` file
- Add `LICENSE.md` to both Snow and SnowFlat theme
- Use UNIX file format, instead of DOS, in some files
- Update `README.md`
- Substitute HTTPS for HTTP in some links (when possible)
- Check for new versions in GitLab clone
- Fix a PHP notice shown when posting answers
- Use documented order for implode()'s parameters
