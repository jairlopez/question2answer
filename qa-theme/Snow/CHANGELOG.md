# CHANGELOG

## 1.4-revision.1.0.0

- Use GPLv3+, instead of GPLv2+
- Include missing copyright notice
- Include missing lines in some copyright notices
