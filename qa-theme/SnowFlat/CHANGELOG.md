# CHANGELOG

## 1.5-revision.1.0.0

- Remove fonts that aren't compatible with GPL
    - Replace Fontello with Font Awesome
    - Offload Font Awesome loading onto CloudFlare's CDN
    - Offload Ubuntu font loading onto Google Fonts
- Add `LICENSE.md`
- Include missing lines in copyright notice
