<?php
/*
	This is a modified version (see CHANGELOG.md) of:

	Question2Answer - Q&A platform - https://www.question2answer.org/
	Copyright (C) 2011-2020 Gideon Greenspan and contributors

	Description: User cookie management (application level) for tracking anonymous posts


	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see
	<https://gitlab.com/jairlopez/question2answer/-/blob/master/LICENSE.md>.
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../../');
	exit;
}


/**
 * Return the user identification cookie sent by the browser for this page request, or null if none
 */
function qa_cookie_get()
{
	if (qa_to_override(__FUNCTION__)) { $args=func_get_args(); return qa_call_override(__FUNCTION__, $args); }

	return isset($_COOKIE['qa_id']) ? qa_gpc_to_string($_COOKIE['qa_id']) : null;
}


/**
 * Return user identification cookie sent by browser if valid, or create a new one if not.
 * Either way, extend for another year (this is used when an anonymous post is created)
 */
function qa_cookie_get_create()
{
	if (qa_to_override(__FUNCTION__)) { $args=func_get_args(); return qa_call_override(__FUNCTION__, $args); }

	require_once QA_INCLUDE_DIR . 'db/cookies.php';

	$cookieid = qa_cookie_get();

	if (!isset($cookieid) || !qa_db_cookie_exists($cookieid)) {
		// cookie is invalid
		$cookieid = qa_db_cookie_create(qa_remote_ip_address());
	}

	setcookie('qa_id', $cookieid, time() + 86400 * 365, '/', QA_COOKIE_DOMAIN, (bool)ini_get('session.cookie_secure'), true);
	$_COOKIE['qa_id'] = $cookieid;

	return $cookieid;
}


/**
 * Called after a database write $action performed by a user identified by $cookieid,
 * relating to $questionid, $answerid and/or $commentid
 * @param $cookieid
 * @param $action
 */
function qa_cookie_report_action($cookieid, $action)
{
	require_once QA_INCLUDE_DIR . 'db/cookies.php';

	qa_db_cookie_written($cookieid, qa_remote_ip_address());
}
