<?php
/*
	This is a modified version (see CHANGELOG.md) of:

	Question2Answer - Q&A platform - https://www.question2answer.org/
	Copyright (C) 2011-2020 Gideon Greenspan and contributors

	Description: Widget module class for activity count plugin


	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see
	<https://gitlab.com/jairlopez/question2answer/-/blob/master/LICENSE.md>.
*/

class qa_category_list
{
	private $themeobject;

	public function allow_template($template)
	{
		return true;
	}

	public function allow_region($region)
	{
		return $region == 'side';
	}

	public function output_widget($region, $place, $themeobject, $template, $request, $qa_content)
	{
		$this->themeobject = $themeobject;

		if (isset($qa_content['navigation']['cat'])) {
			$nav = $qa_content['navigation']['cat'];
		} else {
			$selectspec = qa_db_category_nav_selectspec(null, true, false, true);
			$selectspec['caching'] = array(
				'key' => 'qa_db_category_nav_selectspec:default:full',
				'ttl' => qa_opt('caching_catwidget_time'),
			);
			$navcategories = qa_db_single_select($selectspec);
			$nav = qa_category_navigation($navcategories);
		}

		$this->themeobject->output('<h2>' . qa_lang_html('main/nav_categories') . '</h2>');
		$this->themeobject->set_context('nav_type', 'cat');
		$this->themeobject->nav_list($nav, 'nav-cat', 1);
		$this->themeobject->nav_clear('cat');
		$this->themeobject->clear_context('nav_type');
	}
}
