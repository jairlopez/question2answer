<?php
/*
	This is a modified version (see CHANGELOG.md) of:

	Question2Answer - Q&A platform - https://www.question2answer.org/
	Copyright (C) 2011-2020 Gideon Greenspan and contributors

	Description: Server-side response to Ajax mailing loop requests


	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see
	<https://gitlab.com/jairlopez/question2answer/-/blob/master/LICENSE.md>.
*/

require_once QA_INCLUDE_DIR . 'app/users.php';
require_once QA_INCLUDE_DIR . 'app/mailing.php';


$continue = false;

if (qa_get_logged_in_level() >= QA_USER_LEVEL_ADMIN) {
	$starttime = time();

	qa_mailing_perform_step();

	if ($starttime == time())
		sleep(1); // make sure at least one second has passed

	$message = qa_mailing_progress_message();

	if (isset($message))
		$continue = true;
	else
		$message = qa_lang('admin/mailing_complete');

} else
	$message = qa_lang('admin/no_privileges');


echo "QA_AJAX_RESPONSE\n" . (int)$continue . "\n" . qa_html($message);
