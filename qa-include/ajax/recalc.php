<?php
/*
	This is a modified version (see CHANGELOG.md) of:

	Question2Answer - Q&A platform - https://www.question2answer.org/
	Copyright (C) 2011-2020 Gideon Greenspan and contributors

	Description: Server-side response to Ajax admin recalculation requests


	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see
	<https://gitlab.com/jairlopez/question2answer/-/blob/master/LICENSE.md>.
*/

require_once QA_INCLUDE_DIR . 'app/users.php';
require_once QA_INCLUDE_DIR . 'app/recalc.php';


if (qa_get_logged_in_level() >= QA_USER_LEVEL_ADMIN) {
	if (!qa_check_form_security_code('admin/recalc', qa_post_text('code'))) {
		$state = '';
		$message = qa_lang('misc/form_security_reload');

	} else {
		$state = qa_post_text('state');
		$stoptime = time() + 3;

		while (qa_recalc_perform_step($state) && time() < $stoptime) {
			// wait
		}

		$message = qa_recalc_get_message($state);
	}

} else {
	$state = '';
	$message = qa_lang('admin/no_privileges');
}


echo "QA_AJAX_RESPONSE\n1\n" . $state . "\n" . qa_html($message);
