<?php
/*
	This is a modified version (see CHANGELOG.md) of:

	Question2Answer - Q&A platform - https://www.question2answer.org/
	Copyright (C) 2011-2020 Gideon Greenspan and contributors

	Description: Server-side response to Ajax category information requests


	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see
	<https://gitlab.com/jairlopez/question2answer/-/blob/master/LICENSE.md>.
*/

require_once QA_INCLUDE_DIR . 'db/selects.php';


$categoryid = qa_post_text('categoryid');
if (!strlen((string) $categoryid))
	$categoryid = null;

list($fullcategory, $categories) = qa_db_select_with_pending(
	qa_db_full_category_selectspec($categoryid, true),
	qa_db_category_sub_selectspec($categoryid)
);

echo "QA_AJAX_RESPONSE\n1\n";

echo qa_html(strtr($fullcategory['content'] ?? '', "\r\n", '  ')); // category description

foreach ($categories as $category) {
	// subcategory information
	echo "\n" . $category['categoryid'] . '/' . $category['title'];
}
