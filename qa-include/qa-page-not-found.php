<?php
/*
	This is a modified version (see CHANGELOG.md) of:

	Question2Answer - Q&A platform - https://www.question2answer.org/
	Copyright (C) 2011-2020 Gideon Greenspan and contributors

	Description: Controller for page not found (error 404)


	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see
	<https://gitlab.com/jairlopez/question2answer/-/blob/master/LICENSE.md>.
*/

if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
	header('Location: ../');
	exit;
}

require_once QA_INCLUDE_DIR . 'app/format.php';


header('HTTP/1.0 404 Not Found');

qa_set_template('not-found');

$qa_content = qa_content_prepare();
$qa_content['error'] = qa_lang_html('main/page_not_found');
$qa_content['suggest_next'] = qa_html_suggest_qs_tags(qa_using_tags());


return $qa_content;
